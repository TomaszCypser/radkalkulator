﻿namespace Kalkulator
{
    partial class FormCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxEquation = new System.Windows.Forms.TextBox();
            this.buttonNumb7 = new System.Windows.Forms.Button();
            this.buttonNumb8 = new System.Windows.Forms.Button();
            this.buttonNumb9 = new System.Windows.Forms.Button();
            this.buttonNumb6 = new System.Windows.Forms.Button();
            this.buttonNumb5 = new System.Windows.Forms.Button();
            this.buttonNumb4 = new System.Windows.Forms.Button();
            this.buttonNumb3 = new System.Windows.Forms.Button();
            this.buttonNumb2 = new System.Windows.Forms.Button();
            this.buttonNumb1 = new System.Windows.Forms.Button();
            this.buttonMult = new System.Windows.Forms.Button();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.buttonBackspace = new System.Windows.Forms.Button();
            this.buttonSub = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonEq = new System.Windows.Forms.Button();
            this.buttonNumb0 = new System.Windows.Forms.Button();
            this.label10Sys = new System.Windows.Forms.Label();
            this.label9Sys = new System.Windows.Forms.Label();
            this.label8Sys = new System.Windows.Forms.Label();
            this.label7Sys = new System.Windows.Forms.Label();
            this.label6Sys = new System.Windows.Forms.Label();
            this.label5Sys = new System.Windows.Forms.Label();
            this.label4Sys = new System.Windows.Forms.Label();
            this.label3Sys = new System.Windows.Forms.Label();
            this.label2Sys = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxNumber
            // 
            this.textBoxNumber.Location = new System.Drawing.Point(173, 24);
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.Size = new System.Drawing.Size(584, 20);
            this.textBoxNumber.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bin";
            this.label1.Click += new System.EventHandler(this.System_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ter";
            this.label2.Click += new System.EventHandler(this.System_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Quat";
            this.label3.Click += new System.EventHandler(this.System_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Quin";
            this.label4.Click += new System.EventHandler(this.System_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Senar";
            this.label5.Click += new System.EventHandler(this.System_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Sept";
            this.label6.Click += new System.EventHandler(this.System_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Oct";
            this.label7.Click += new System.EventHandler(this.System_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Nov";
            this.label8.Click += new System.EventHandler(this.System_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Dec";
            this.label9.Click += new System.EventHandler(this.System_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(439, 57);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(156, 23);
            this.buttonClear.TabIndex = 10;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxEquation
            // 
            this.textBoxEquation.Location = new System.Drawing.Point(12, 24);
            this.textBoxEquation.Name = "textBoxEquation";
            this.textBoxEquation.Size = new System.Drawing.Size(155, 20);
            this.textBoxEquation.TabIndex = 11;
            // 
            // buttonNumb7
            // 
            this.buttonNumb7.Location = new System.Drawing.Point(439, 86);
            this.buttonNumb7.Name = "buttonNumb7";
            this.buttonNumb7.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb7.TabIndex = 13;
            this.buttonNumb7.Text = "7";
            this.buttonNumb7.UseVisualStyleBackColor = true;
            this.buttonNumb7.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb8
            // 
            this.buttonNumb8.Location = new System.Drawing.Point(520, 86);
            this.buttonNumb8.Name = "buttonNumb8";
            this.buttonNumb8.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb8.TabIndex = 14;
            this.buttonNumb8.Text = "8";
            this.buttonNumb8.UseVisualStyleBackColor = true;
            this.buttonNumb8.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb9
            // 
            this.buttonNumb9.Location = new System.Drawing.Point(601, 86);
            this.buttonNumb9.Name = "buttonNumb9";
            this.buttonNumb9.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb9.TabIndex = 15;
            this.buttonNumb9.Text = "9";
            this.buttonNumb9.UseVisualStyleBackColor = true;
            this.buttonNumb9.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb6
            // 
            this.buttonNumb6.Location = new System.Drawing.Point(601, 115);
            this.buttonNumb6.Name = "buttonNumb6";
            this.buttonNumb6.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb6.TabIndex = 18;
            this.buttonNumb6.Text = "6";
            this.buttonNumb6.UseVisualStyleBackColor = true;
            this.buttonNumb6.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb5
            // 
            this.buttonNumb5.Location = new System.Drawing.Point(520, 115);
            this.buttonNumb5.Name = "buttonNumb5";
            this.buttonNumb5.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb5.TabIndex = 17;
            this.buttonNumb5.Text = "5";
            this.buttonNumb5.UseVisualStyleBackColor = true;
            this.buttonNumb5.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb4
            // 
            this.buttonNumb4.Location = new System.Drawing.Point(439, 115);
            this.buttonNumb4.Name = "buttonNumb4";
            this.buttonNumb4.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb4.TabIndex = 16;
            this.buttonNumb4.Text = "4";
            this.buttonNumb4.UseVisualStyleBackColor = true;
            this.buttonNumb4.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb3
            // 
            this.buttonNumb3.Location = new System.Drawing.Point(601, 145);
            this.buttonNumb3.Name = "buttonNumb3";
            this.buttonNumb3.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb3.TabIndex = 21;
            this.buttonNumb3.Text = "3";
            this.buttonNumb3.UseVisualStyleBackColor = true;
            this.buttonNumb3.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb2
            // 
            this.buttonNumb2.Location = new System.Drawing.Point(520, 145);
            this.buttonNumb2.Name = "buttonNumb2";
            this.buttonNumb2.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb2.TabIndex = 20;
            this.buttonNumb2.Text = "2";
            this.buttonNumb2.UseVisualStyleBackColor = true;
            this.buttonNumb2.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonNumb1
            // 
            this.buttonNumb1.Location = new System.Drawing.Point(439, 145);
            this.buttonNumb1.Name = "buttonNumb1";
            this.buttonNumb1.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb1.TabIndex = 19;
            this.buttonNumb1.Text = "1";
            this.buttonNumb1.UseVisualStyleBackColor = true;
            this.buttonNumb1.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // buttonMult
            // 
            this.buttonMult.Location = new System.Drawing.Point(682, 87);
            this.buttonMult.Name = "buttonMult";
            this.buttonMult.Size = new System.Drawing.Size(75, 23);
            this.buttonMult.TabIndex = 24;
            this.buttonMult.Text = "*";
            this.buttonMult.UseVisualStyleBackColor = true;
            this.buttonMult.Click += new System.EventHandler(this.buttonOperand_Click);
            // 
            // buttonDiv
            // 
            this.buttonDiv.Location = new System.Drawing.Point(682, 57);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(75, 23);
            this.buttonDiv.TabIndex = 23;
            this.buttonDiv.Text = "/";
            this.buttonDiv.UseVisualStyleBackColor = true;
            this.buttonDiv.Click += new System.EventHandler(this.buttonOperand_Click);
            // 
            // buttonBackspace
            // 
            this.buttonBackspace.Location = new System.Drawing.Point(601, 57);
            this.buttonBackspace.Name = "buttonBackspace";
            this.buttonBackspace.Size = new System.Drawing.Size(75, 23);
            this.buttonBackspace.TabIndex = 22;
            this.buttonBackspace.Text = "Backspace";
            this.buttonBackspace.UseVisualStyleBackColor = true;
            this.buttonBackspace.Click += new System.EventHandler(this.buttonBackspace_Click);
            // 
            // buttonSub
            // 
            this.buttonSub.Location = new System.Drawing.Point(682, 116);
            this.buttonSub.Name = "buttonSub";
            this.buttonSub.Size = new System.Drawing.Size(75, 23);
            this.buttonSub.TabIndex = 25;
            this.buttonSub.Text = "-";
            this.buttonSub.UseVisualStyleBackColor = true;
            this.buttonSub.Click += new System.EventHandler(this.buttonOperand_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(682, 145);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 26;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonOperand_Click);
            // 
            // buttonEq
            // 
            this.buttonEq.Location = new System.Drawing.Point(682, 174);
            this.buttonEq.Name = "buttonEq";
            this.buttonEq.Size = new System.Drawing.Size(75, 23);
            this.buttonEq.TabIndex = 28;
            this.buttonEq.Text = "=";
            this.buttonEq.UseVisualStyleBackColor = true;
            this.buttonEq.Click += new System.EventHandler(this.buttonEq_Click);
            // 
            // buttonNumb0
            // 
            this.buttonNumb0.Location = new System.Drawing.Point(520, 174);
            this.buttonNumb0.Name = "buttonNumb0";
            this.buttonNumb0.Size = new System.Drawing.Size(75, 23);
            this.buttonNumb0.TabIndex = 27;
            this.buttonNumb0.Text = "0";
            this.buttonNumb0.UseVisualStyleBackColor = true;
            this.buttonNumb0.Click += new System.EventHandler(this.buttonNumber_Click);
            // 
            // label10Sys
            // 
            this.label10Sys.AutoSize = true;
            this.label10Sys.Location = new System.Drawing.Point(52, 249);
            this.label10Sys.Name = "label10Sys";
            this.label10Sys.Size = new System.Drawing.Size(0, 13);
            this.label10Sys.TabIndex = 37;
            // 
            // label9Sys
            // 
            this.label9Sys.AutoSize = true;
            this.label9Sys.Location = new System.Drawing.Point(52, 225);
            this.label9Sys.Name = "label9Sys";
            this.label9Sys.Size = new System.Drawing.Size(0, 13);
            this.label9Sys.TabIndex = 36;
            // 
            // label8Sys
            // 
            this.label8Sys.AutoSize = true;
            this.label8Sys.Location = new System.Drawing.Point(52, 203);
            this.label8Sys.Name = "label8Sys";
            this.label8Sys.Size = new System.Drawing.Size(0, 13);
            this.label8Sys.TabIndex = 35;
            // 
            // label7Sys
            // 
            this.label7Sys.AutoSize = true;
            this.label7Sys.Location = new System.Drawing.Point(52, 179);
            this.label7Sys.Name = "label7Sys";
            this.label7Sys.Size = new System.Drawing.Size(0, 13);
            this.label7Sys.TabIndex = 34;
            // 
            // label6Sys
            // 
            this.label6Sys.AutoSize = true;
            this.label6Sys.Location = new System.Drawing.Point(52, 155);
            this.label6Sys.Name = "label6Sys";
            this.label6Sys.Size = new System.Drawing.Size(0, 13);
            this.label6Sys.TabIndex = 33;
            // 
            // label5Sys
            // 
            this.label5Sys.AutoSize = true;
            this.label5Sys.Location = new System.Drawing.Point(52, 132);
            this.label5Sys.Name = "label5Sys";
            this.label5Sys.Size = new System.Drawing.Size(0, 13);
            this.label5Sys.TabIndex = 32;
            // 
            // label4Sys
            // 
            this.label4Sys.AutoSize = true;
            this.label4Sys.Location = new System.Drawing.Point(52, 109);
            this.label4Sys.Name = "label4Sys";
            this.label4Sys.Size = new System.Drawing.Size(0, 13);
            this.label4Sys.TabIndex = 31;
            // 
            // label3Sys
            // 
            this.label3Sys.AutoSize = true;
            this.label3Sys.Location = new System.Drawing.Point(52, 85);
            this.label3Sys.Name = "label3Sys";
            this.label3Sys.Size = new System.Drawing.Size(0, 13);
            this.label3Sys.TabIndex = 30;
            // 
            // label2Sys
            // 
            this.label2Sys.AutoSize = true;
            this.label2Sys.Location = new System.Drawing.Point(53, 62);
            this.label2Sys.Name = "label2Sys";
            this.label2Sys.Size = new System.Drawing.Size(0, 13);
            this.label2Sys.TabIndex = 29;
            // 
            // FormCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 311);
            this.Controls.Add(this.label10Sys);
            this.Controls.Add(this.label9Sys);
            this.Controls.Add(this.label8Sys);
            this.Controls.Add(this.label7Sys);
            this.Controls.Add(this.label6Sys);
            this.Controls.Add(this.label5Sys);
            this.Controls.Add(this.label4Sys);
            this.Controls.Add(this.label3Sys);
            this.Controls.Add(this.label2Sys);
            this.Controls.Add(this.buttonEq);
            this.Controls.Add(this.buttonNumb0);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonSub);
            this.Controls.Add(this.buttonMult);
            this.Controls.Add(this.buttonDiv);
            this.Controls.Add(this.buttonBackspace);
            this.Controls.Add(this.buttonNumb3);
            this.Controls.Add(this.buttonNumb2);
            this.Controls.Add(this.buttonNumb1);
            this.Controls.Add(this.buttonNumb6);
            this.Controls.Add(this.buttonNumb5);
            this.Controls.Add(this.buttonNumb4);
            this.Controls.Add(this.buttonNumb9);
            this.Controls.Add(this.buttonNumb8);
            this.Controls.Add(this.buttonNumb7);
            this.Controls.Add(this.textBoxEquation);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxNumber);
            this.Name = "FormCalc";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxEquation;
        private System.Windows.Forms.Button buttonNumb7;
        private System.Windows.Forms.Button buttonNumb8;
        private System.Windows.Forms.Button buttonNumb9;
        private System.Windows.Forms.Button buttonNumb6;
        private System.Windows.Forms.Button buttonNumb5;
        private System.Windows.Forms.Button buttonNumb4;
        private System.Windows.Forms.Button buttonNumb3;
        private System.Windows.Forms.Button buttonNumb2;
        private System.Windows.Forms.Button buttonNumb1;
        private System.Windows.Forms.Button buttonMult;
        private System.Windows.Forms.Button buttonDiv;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonSub;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonEq;
        private System.Windows.Forms.Button buttonNumb0;
        private System.Windows.Forms.Label label10Sys;
        private System.Windows.Forms.Label label9Sys;
        private System.Windows.Forms.Label label8Sys;
        private System.Windows.Forms.Label label7Sys;
        private System.Windows.Forms.Label label6Sys;
        private System.Windows.Forms.Label label5Sys;
        private System.Windows.Forms.Label label4Sys;
        private System.Windows.Forms.Label label3Sys;
        private System.Windows.Forms.Label label2Sys;
    }
}

