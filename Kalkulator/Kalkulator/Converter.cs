﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    class Converter
    {
        public static string ConvertFromToSystem(string value, int systemBase, int systemDest)
        {
            try
            {
                if (systemBase == systemDest)
                {
                    return value;
                }
                else if (systemBase == 10)
                {
                    return DecimalToArbitrarySystem(Convert.ToInt64(Math.Floor(Convert.ToDouble(value))), systemDest);
                }
                else if (systemDest == 10)
                {
                    return ArbitraryToDecimalSystem(value, systemBase).ToString();
                }
                else
                {
                    long result = ArbitraryToDecimalSystem(value, systemBase);
                    return DecimalToArbitrarySystem(result, systemDest);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Podano za dużą wartość ! Program zakończy działanie");
                System.Environment.Exit(1);
                return "";
            }
        }

        private static string DecimalToArbitrarySystem(long decimalNumber, int system)
        {
            const int BitsInLong = 64;
            const string Digits = "0123456789";

            if (system < 2 || system > Digits.Length)
                throw new ArgumentException("The system must be between 2 and between " + Digits.Length.ToString());

            if (decimalNumber == 0)
                return "0";

            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];

            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % system);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / system;
            }

            string result = new String(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0)
            {
                result = "-" + result;
            }

            return result;
        }

        private static long ArbitraryToDecimalSystem(string number, int system)
        {
            const string Digits = "0123456789";

            if (system < 2 || system > Digits.Length)
                throw new ArgumentException("The system must be between 2 and " +
                    Digits.Length.ToString());

            if (String.IsNullOrEmpty(number))
                return 0;

            number = number.ToUpperInvariant();

            long result = 0;
            long multiplier = 1;
            for (int i = number.Length - 1; i >= 0; i--)
            {
                char c = number[i];
                if (i == 0 && c == '-')
                {
                    result = -result;
                    break;
                }

                int digit = Digits.IndexOf(c);
                if (digit == -1)
                    throw new ArgumentException(
                        "Invalid character in the arbitrary numeral system number",
                        "number");

                result += digit * multiplier;
                multiplier *= system;
            }

            return result;
        }
    }
}
