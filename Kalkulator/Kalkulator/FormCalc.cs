﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;


namespace Kalkulator
{
    public partial class FormCalc : Form
    {
        string operand = "";
        int system = 10;

        public FormCalc()
        {
            InitializeComponent();
            textBoxEquation.ReadOnly = true;
            textBoxNumber.ReadOnly = true;
        }

        private void buttonNumber_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (textBoxEquation.Text != "")
            {
                string[] listEnding = new string[] { "*", "+", "/", "-" };
                if (!(listEnding.Any(x => textBoxEquation.Text.EndsWith(x))) && operand == "")
                {
                    textBoxEquation.Text = "";
                }
            }
            textBoxNumber.Text += button.Text;
            label2Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 2);
            label3Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 3);
            label4Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 4);
            label5Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 5);
            label6Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 6);
            label7Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 7);
            label8Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 8);
            label9Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 9);
            label10Sys.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, system, 10);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxNumber.Text = ""; 
            operand = "";
            textBoxEquation.Text = "";
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            string str = textBoxNumber.Text;
            if(str == "")
            {
                return;
            }
            str = str.Remove(str.Length - 1, 1);
            textBoxNumber.Text = str;
        }

        private void buttonOperand_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if(textBoxEquation.Text == "" && textBoxNumber.Text == "")
            {
                return;
            }
            if(operand != "")
            {
                if(textBoxNumber.Text != "")
                {
                    buttonEq_Click(sender, e);
                    return;
                }
                string str = textBoxEquation.Text;
                str = str.Remove(str.Length - 1, 1) + button.Text;
                textBoxNumber.Text = "";
                textBoxEquation.Text = str;
                operand = button.Text;
            }
            else
            {
                if (textBoxEquation.Text != "")
                {
                    operand = button.Text;
                    textBoxEquation.Text = textBoxEquation.Text + operand;
                    textBoxNumber.Text = "";
                }
                else
                {
                    operand = button.Text;
                    textBoxEquation.Text = textBoxNumber.Text + operand;
                    textBoxNumber.Text = "";
                }
            }
        }

        private void buttonEq_Click(object sender, EventArgs e)
        {
            string math = textBoxEquation.Text + textBoxNumber.Text;
            if(textBoxEquation.Text =="" || textBoxNumber.Text == "")
            {
                return;
            }
            try
            {
                string value = new DataTable().Compute(math, null).ToString();
                if(value == "\u221E")
                {
                    MessageBox.Show("Błąd dzielenia przez 0");
                    System.Environment.Exit(1);
                }
                value = Convert.ToInt64(Math.Floor(Convert.ToDouble(value))).ToString();
                label2Sys.Text = Converter.ConvertFromToSystem(value, system, 2);
                label3Sys.Text = Converter.ConvertFromToSystem(value, system, 3);
                label4Sys.Text = Converter.ConvertFromToSystem(value, system, 4);
                label5Sys.Text = Converter.ConvertFromToSystem(value, system, 5);
                label6Sys.Text = Converter.ConvertFromToSystem(value, system, 6);
                label7Sys.Text = Converter.ConvertFromToSystem(value, system, 7);
                label8Sys.Text = Converter.ConvertFromToSystem(value, system, 8);
                label9Sys.Text = Converter.ConvertFromToSystem(value, system, 9);
                label10Sys.Text = Converter.ConvertFromToSystem(value, system, 10);
                textBoxEquation.Text = value;
                textBoxNumber.Text = "";
                operand = "";
            }
            catch(Exception )
            {
                MessageBox.Show("Podano za dużą wartość ! Program zakończy działanie" );
                System.Environment.Exit(1);
            }
        }

        private void System_Click(object sender, EventArgs e)
        {
            int tempSystem = system;
            Label label = (Label)sender;
            foreach (Control c in this.Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    var l = (Label)c;
                    if (label.Text == l.Text)
                    {
                        l.Font = new Font(label.Font, FontStyle.Bold);
                        switch (l.Text)
                        {
                            case "Bin":
                                system = 2;
                                break;
                            case "Ter":
                                system = 3;
                                break;
                            case "Quat":
                                system = 4;
                                break;
                            case "Quin":
                                system = 5;
                                break;
                            case "Senar":
                                system = 6;
                                break;
                            case "Sept":
                                system = 7;
                                break;
                            case "Oct":
                                system = 8;
                                break;
                            case "Nov":
                                system = 9;
                                break;
                            case "Dec":
                                system = 10;
                                break;
                        }
                    }
                    else
                    {
                        l.Font = new Font(label.Font, FontStyle.Regular);
                    }
                }
            }
            if(textBoxEquation.Text != "")
            {
                string[] listEnding = new string[] { "*", "+", "/", "-" };
                if ((listEnding.Any(x => textBoxEquation.Text.EndsWith(x))))
                {
                    string str = textBoxEquation.Text;
                    str = str.Remove(str.Length - 1, 1);
                    textBoxEquation.Text = str;
                }
                textBoxEquation.Text= Converter.ConvertFromToSystem(textBoxEquation.Text, tempSystem, system) + operand;
            }
            if (textBoxNumber.Text != "")
            {
                textBoxNumber.Text = Converter.ConvertFromToSystem(textBoxNumber.Text, tempSystem, system);
            }
            foreach (Control c in this.Controls)
            {
                if (c.GetType() == typeof(Button))
                {
                    var button = (Button)c;
                    int n;
                    bool isNumeric = int.TryParse(button.Text, out n);
                    if (isNumeric)
                    {
                        if (system <= Convert.ToInt32(button.Text))
                        {
                            button.Enabled = false;
                        }
                        else
                        {
                            button.Enabled = true;
                        }
                    }
                }
            }
        }
    }
}
